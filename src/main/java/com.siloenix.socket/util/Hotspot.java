package com.siloenix.socket.util;

import java.io.IOException;
import java.net.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Hotspot {
    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public Future<String> scan() throws SocketException {
        System.out.println("scanning...");
        DatagramSocket socket = new DatagramSocket(8000);
        socket.setSoTimeout(5000);
        byte[] address = new byte[1];
        DatagramPacket packet = new DatagramPacket(address, 1);
        return executorService.submit(() -> {
            try {
                socket.receive(packet);
            } catch (IOException e) {
                System.out.println("no server on network");
                return null;
            } finally {
                socket.close();
            }
            System.out.println("server found: " + packet.getAddress().getCanonicalHostName());
            return packet.getAddress().getCanonicalHostName();
        });
    }

    public void broadcast() throws SocketException, UnknownHostException {
        DatagramSocket socket = new DatagramSocket(8001);
        socket.setBroadcast(true);
        DatagramPacket packet = new DatagramPacket(new byte[1], 1);
        socket.connect(InetAddress.getByName("255.255.255.255"), 8000);
        executorService.submit(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    break;
                }
            }
            System.out.println("hotspot closed");
        });
    }

    public void shutdown() {
        executorService.shutdownNow();
    }
}
