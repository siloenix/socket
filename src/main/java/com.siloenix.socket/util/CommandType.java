package com.siloenix.socket.util;

public enum CommandType {
    NAME, STAT, CLOS, ECHO
}
