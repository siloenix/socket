package com.siloenix.socket.util;

public class CommandTranslator {
    public static final String NAME_COMMAND = "name ";
    public static final String STAT_COMMAND = "stat";
    public static final String CLOS_COMMAND = "clos";

    public static Command translate(String message) {
        if (message.startsWith(NAME_COMMAND)) {
            return new Command(
                    CommandType.NAME,
                    message.replace(NAME_COMMAND, "")
            );
        }
        if (message.equals(STAT_COMMAND)) {
            return new Command(CommandType.STAT);
        }
        if (message.equals(CLOS_COMMAND)) {
            return new Command(CommandType.CLOS);
        }
        return new Command(CommandType.ECHO, message);
    }

}
