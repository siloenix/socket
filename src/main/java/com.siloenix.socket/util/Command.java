package com.siloenix.socket.util;


public class Command {
    private CommandType type;
    private String payload = null;

    public Command(CommandType type) {
        this.type = type;
    }

    public Command(CommandType type, String payload) {
        this.type = type;
        this.payload = payload;
    }

    public CommandType type() {
        return type;
    }

    public String payload() {
        return payload;
    }
}
