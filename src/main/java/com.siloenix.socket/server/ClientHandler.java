package com.siloenix.socket.server;

import java.nio.channels.SocketChannel;

public class ClientHandler {
    private String name;
    private SocketChannel connection;

    public ClientHandler(String name, SocketChannel connection) {
        this.name = name;
        this.connection = connection;
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SocketChannel connection() {
        return connection;
    }
}
