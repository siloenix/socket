package com.siloenix.socket.server;

import com.siloenix.socket.util.Command;
import com.siloenix.socket.util.CommandTranslator;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Server {
    private ClientManager clientManager = new ClientManager();

    private ServerSocketChannel server;
    private SocketChannel currentClient;

    public void start() throws Exception {
        Selector selector = Selector.open();
        server = ServerSocketChannel.open();
        server.configureBlocking(false);
        server.socket().bind(new InetSocketAddress("0.0.0.0", 9000));
        server.register(selector, SelectionKey.OP_ACCEPT);

        System.out.println("server started");
        while (!Thread.currentThread().isInterrupted()) {
            if (selector.select() <= 0) {
                continue;
            }
            processReadySet(selector.selectedKeys());
        }
        System.out.println("server closed");
    }
    private void processReadySet(Set readySet) throws Exception {
        Iterator iterator = readySet.iterator();
        while (iterator.hasNext()) {
            SelectionKey key = (SelectionKey) iterator.next();
            iterator.remove();

            if (key.isAcceptable()) {
                processNewClient(key);
            }

            if (key.isReadable()) {
                currentClient = (SocketChannel) key.channel();
                String msg = processRead();
                log(msg);
                String response = executeCommand(CommandTranslator.translate(msg));
                if (response != null && response.length() > 0) {
                    processWrite(key, response);
                }
            }
        }
    }

    private void processNewClient(SelectionKey key) throws IOException {
        ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();
        currentClient = serverChannel.accept();
        currentClient
                .configureBlocking(false)
                .register(key.selector(), SelectionKey.OP_READ);

        clientManager.registerClient(currentClient);
        log("connected");
    }

    private String processRead() throws Exception {
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        int bytesCount = currentClient.read(buffer);
        if (bytesCount > 0) {
            buffer.flip(); // for reading after writing
            byte[] str = buffer.array();
            return new String(str, 0, bytesCount);
        }
        return "no message";
    }

    private void processWrite(SelectionKey key, String msg) throws IOException {
        try {
            ByteBuffer buffer = ByteBuffer.wrap(msg.getBytes());
            currentClient.write(buffer);
        } catch (IOException e) {
            clientManager.disconnectClient(currentClient);
            key.cancel();
        }
    }

    private String executeCommand(Command command) throws IOException {
        switch (command.type()) {
            case NAME:
                clientManager.setClientName(currentClient, command.payload());
                return CommandTranslator.NAME_COMMAND + command.payload();
            case STAT:
                return CommandTranslator.STAT_COMMAND + " " + clientManager.getAllClients();
            case CLOS:
                clientManager.disconnectClients();
                server.close();
                Thread.currentThread().interrupt();
                return null;
            case ECHO:
                return command.payload();
        }
        return null;
    }

    private void log(String message) {
        System.out.println(clientManager.getClient(currentClient).name() + ": " + message);
    }
}
