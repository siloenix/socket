package com.siloenix.socket.server;

import java.io.IOException;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;

public class ClientManager {
    private Map<Integer, ClientHandler> clients = new HashMap<>();

    public void registerClient(SocketChannel clientChannel) {
        int port = clientChannel.socket().getPort();
        clients.put(
                port,
                new ClientHandler("port:"+port, clientChannel)
        );
    }

    public void setClientName(SocketChannel clientChannel, String name) {
        getClient(clientChannel).setName(name);
    }

    public String getAllClients() {
        StringBuilder sb = new StringBuilder();
        clients.forEach((i, c) -> sb.append(c.name()).append(":")); // concatenate client names
        sb.replace(sb.length() - 1, sb.length(), ""); // remove last ":"
        return sb.toString();
    }

    public void disconnectClients() throws IOException {
        for (ClientHandler client : clients.values()) {
            disconnectClient(client);
        }
    }

    public void disconnectClient(ClientHandler client) throws IOException {
        client.connection().close();
        System.out.println(client.name() + " disconnected");
    }

    public void disconnectClient(SocketChannel clientChannel) throws IOException {
        ClientHandler client = getClient(clientChannel);
        disconnectClient(client);
        removeClient(clientChannel);
    }

    public ClientHandler getClient(SocketChannel clientChannel) {
        return clients.get(clientChannel.socket().getPort());
    }

    public void removeClient(SocketChannel clientChannel) {
        clients.remove(clientChannel.socket().getPort());
    }
}
