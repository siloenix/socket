package com.siloenix.socket.client;

import com.siloenix.socket.util.CommandTranslator;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private DataInputStream input;
    private DataOutputStream output;
    private Scanner console = new Scanner(System.in);
    private Socket socket = new Socket();


    public void start(String host, int port) throws Exception{
        socket.connect(new InetSocketAddress(host, port));
        input = new DataInputStream(socket.getInputStream());
        output = new DataOutputStream(socket.getOutputStream());

        setClientName();

        processCommands();
    }

    private void processCommands() throws IOException {
        while (!Thread.currentThread().isInterrupted()) {
            String line = console.nextLine();
            if (line.equals("exit")) {
                break;
            }
            send(line);
            receive();
        }
        socket.close();
        System.out.println("connection closed");
    }

    private void setClientName() throws IOException {
        System.out.println("Enter your name:");
        String name = console.nextLine();
        send(CommandTranslator.NAME_COMMAND + name);
        receive();
    }

    private void send(String message) throws IOException {
        output.write(message.getBytes());
        System.out.println("sent: \"" + message + "\"");
    }

    private void receive() throws IOException {
        byte[] str = new byte[1024];
        int bytesCount = input.read(str);
        if (bytesCount > 0) {
            String s1 = new String(str, 0, bytesCount);
            System.out.println("received: \"" + s1 + "\"");
        } else if (bytesCount < 0) {
            Thread.currentThread().interrupt();
            System.out.println("no connection");
        } else {
            System.out.println("no response");
        }
    }

    public static void main(String[] args) throws Exception{
        new Client().start("localhost", 9000);
    }
}
