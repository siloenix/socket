package com.siloenix.socket;


import com.siloenix.socket.client.Client;
import com.siloenix.socket.server.Server;
import com.siloenix.socket.util.Hotspot;

public class Program {
    public static void main(String[] args) throws Exception {
        Hotspot hotspot = new Hotspot();

        String address = hotspot.scan().get();

        if (address == null) {
            Server server = new Server();
            hotspot.broadcast();
            server.start();
            hotspot.shutdown();
        } else {
            Client client = new Client();
            hotspot.shutdown();
            client.start(address, 9000);
        }
    }

}
